Init:

```
> npm ci
```


Build:

```
> rollup --config
```

This creates ./dist/main.js that can be copied into a Screeps code branch
